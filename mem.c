#include "mem.h"
#include "common.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>
#include <stdbool.h>

// constante définie dans gcc seulement
#ifdef __BIGGEST_ALIGNMENT__
#define ALIGNMENT __BIGGEST_ALIGNMENT__
#else
#define ALIGNMENT 16
#endif

#define ALIGN(val,align)\
	(((__intptr_t)(val) + ((align) - 1))&~((align) - 1))

/* zone libre et zone occupée */
struct fb {
	size_t size;
	bool occupied;  // Vaut true si c'est occupé et false si c'est libre
	struct fb* next;
	struct fb* predecessor;
};

/* structure globale */
typedef struct {
	size_t size;
	size_t meta_size; // taille des meta données
	void *adresse;
}memoire;

memoire mem;


void mem_init(void* memoire, size_t taille) {
	assert(memoire == get_memory_adr());
	assert(taille == get_memory_size());
	mem.adresse = memoire; //init de la structure globale mem
	mem.size = taille;
	mem.meta_size = sizeof(struct fb);
	mem.meta_size = ALIGN(mem.meta_size, ALIGNMENT);
	printf("meta_size = %ld \n",mem.meta_size);
	struct fb *first_fb = (struct fb*)mem.adresse; //Première zone libre
	first_fb->size = mem.size - mem.meta_size;
	first_fb->occupied = 0;
	first_fb->next = NULL;
	first_fb->predecessor = NULL;
	mem_fit(&mem_fit_first); 	// Choix de la méthode pour remplier la zone
}

static mem_fit_function_t *mem_fit_fn;

void mem_fit(mem_fit_function_t *f) {
	mem_fit_fn=f;
}

void *mem_alloc(size_t taille) {
	struct fb *zone=mem_fit_fn( (struct fb*)mem.adresse, taille); //Recherche d'une zone libre
	taille = ALIGN(taille,ALIGNMENT);
	if(taille < zone->size){  // Si la taille = la taille de la zone alors elle devient une zo sinon on créé une autre zone libre
		void *adresse = (void*)zone + taille + mem.meta_size ;
		struct fb *zone_libre = (struct fb*)adresse;
		zone_libre->size = zone->size - taille - mem.meta_size;
		zone_libre->occupied = 0;
		zone_libre->predecessor = zone;
		zone_libre->next = zone->next;
		zone->next = zone_libre;
	}
	zone->size = taille;
	zone->occupied = 1;
	return zone;
}


void mem_free(void* adresse) {  //on a changé le nom de l'argument car mem été déjà prit
	struct fb *zone = (struct fb*)adresse;
	struct fb *predecessor= zone->predecessor;
	struct fb *next = zone->next;
	if ((predecessor == NULL || predecessor->occupied==1)&&(next == NULL || next->occupied==1)) // Cas où predecessor et next sont des zo ou NULL (4 cas possibles)
		zone->occupied = 0;
	else if ( predecessor != NULL && predecessor->occupied == 0 ){  //Cas où predecessor est une zone libre
		predecessor->next = zone->next;
	  predecessor->size = predecessor->size + mem.meta_size + zone->size ;
	  if( next != NULL )
	 		next->predecessor = predecessor;
    if(next != NULL && next->occupied == 0)
		  mem_free((void*)predecessor);
 }
	 else if (next != NULL && next->occupied == 0){  //Cas où next est une zone libre
		 zone->next = next->next;
		 zone->size = zone->size + mem.meta_size + next->size;
		 zone->occupied = 0;
		 if( predecessor != NULL)
				predecessor->next = zone;
	 }
}

void mem_show(void (*print)(void *, size_t, int)) {
	void *current = mem.adresse;
	while (current != NULL) {
		struct fb *first_fb = (struct fb*)current;
		print(current, first_fb->size + mem.meta_size, !first_fb->occupied);  //si c'est une zo, on renvoie les info de la zone sinon on ne renvoi rien
		current = first_fb->next;
	}
}

struct fb* mem_fit_first(struct fb *list, size_t size) { //retourne l’adresse du premier bloc libre de taille supérieure ou égale à size présent dans la liste de blocs libre dont l’adresse est list.
	if (size > mem.size - mem.meta_size){
		printf("ERROR\n");
		return NULL;
	}
	struct fb *current = list;
	while( ((void *)current) < (mem.adresse + mem.size) ){ // tant qu'on ne dépasse pas la fin de la memoire
		if(current->occupied == 0)
			if(current->size >= size + mem.meta_size) // Il faut laisser de la place pour créer les meta de la zone libre
				return current;
		current = current->next ;
	}
	printf("Error\n");
	return NULL;
}

/* Fonction à faire dans un second temps
 * - utilisée par realloc() dans malloc_stub.c
 * - nécessaire pour remplacer l'allocateur de la libc
 * - donc nécessaire pour 'make test_ls'
 * Lire malloc_stub.c pour comprendre son utilisation
 * (ou en discuter avec l'enseignant)
 */
size_t mem_get_size(void *adresse) {
	/* zone est une adresse qui a été retournée par mem_alloc() */
  struct fb *zone = (struct fb*)adresse;
	/* la valeur retournée doit être la taille maximale que
	 * l'utilisateur peut utiliser dans cette zone */
	return zone->size;
}

/* Fonctions facultatives
 * autres stratégies d'allocation
 */
struct fb* mem_fit_best(struct fb *list, size_t size) {
	return NULL;
}

struct fb* mem_fit_worst(struct fb *list, size_t size) {
	return NULL;
}
